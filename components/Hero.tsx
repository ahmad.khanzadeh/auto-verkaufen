"use client";

import React from 'react'
import Image from 'next/image';
import CustomButton from './CustomButton';

// rafce 
const Hero = () => {
  const handleScroll=() =>{}
  return (
    <div className="hero">
      <div className="flex-1 pt-36 padding-x">
        <h1 className="hero__title">
          Suchen Sie ein Buch oder meiten Sie ein Auto! ---Shnell und einfach!
        </h1>
        <p className="hero__subtitle">
        Optimieren Sie Ihr Mietwagenerlebnis mit unserem mühelosen Buchungsprozess
        </p>
        <CustomButton
          title="Suchen Sie die Autos"
          containerStyles="bg-primery-blue mt-10 rounded-full text-white"
          handleClick={handleScroll}
        />
      </div>
      <div className='hero__image-container'>
        <div className="hero__image">
          <Image src="/hero.png" alt="hero" fill className="object-contain" />
        </div>
        <div className="hero__image-overlay"></div>
      </div>
    </div>
  )
}

export default Hero