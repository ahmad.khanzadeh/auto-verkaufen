"use client";
// remember- combobox will now work regualry- will get error
import { Combobox, Transition } from '@headlessui/react';
import { SearchManufacturereProps } from '@/types'
import React from 'react';
import {useState, Fragment} from 'react';
import Image from 'next/image';
import { manufacturers } from '@/constants';

const SearchManufacturer = ({manufacturer,setmanufacturer}:SearchManufacturereProps) => {
  const [query, setquery] = useState('');
  // get the list of all manufacturer to offer in display box
  // the following section is going to clean empty space to make sure that the search item will be displayed to the user
  const filteredManufacturers=
       query ==="" 
         ? manufacturers 
         : manufacturers.filter((item) =>(
          item.toLowerCase().replace(/\s+/g,"").includes(query.toLowerCase().replace(/\s+/g,""))
          // replace all the empty places with a string
         ))
  return (
    <div className="search-manufacturer">
      <Combobox value={manufacturer} onChange={setmanufacturer}>
        <div className="relative w-rull">
          <Combobox.Button className="absolute top-[14px]">
            <Image 
            src='/car-logo.svg'
            width={20}
            height={20}
            className="ml-4"
            alt="car Logo"
            />
          </Combobox.Button>
          <Combobox.Input
             className="search-manufacturer__input"
             placeholder='PORSCHE'
             displayValue={(manufacturer: string)=> manufacturer}
             onChange={(e) => setquery(e.target.value)}
          />
          <Transition
           as={Fragment}
           leave="transition ease-in duration-100"
           leaveFrom='opacity-100'
           leaveTo='opacity-0'
           afterLeave={()=> setquery('')}
          >
             <Combobox.Options>
                  {filteredManufacturers.length ===0 && query !=="" ? (
                    <Combobox.Option
                      value={query}
                      className="search-manufacturer__option"
                    >
                        the input was empty. We have nothing. Create "{query}"
                    </Combobox.Option>
                  ): (
                    filteredManufacturers.map((item) =>(
                      <Combobox.Option
                        key={item}
                        className={({active}) =>` 
                        relative search-manufacturer__option ${active ? 'bg-primary-blue text-white' : 'text-gray-900'}
                        `}
                        value={item}
                      >
                          {/* {item} */}
                          {({selected,active})=>(
                            <>
                             <span
                              className={`block truncate ${selected ? 'font-medium' : 'font-normal'}`}
                             >
                              {item}
                             </span>
                             {selected ?(
                              <span
                               className={`absolute inset-y-0 left-0 flex-items-center pl-3 ${active ? 'text-white' : 'text-teal-600'}`}
                              >
                              </span>
                             ) : null}
                            </>
                    )}
                      </Combobox.Option>
                    ))
                  )}
             </Combobox.Options>
          </Transition>
        </div>
      </Combobox>
    </div>
  )
}

export default SearchManufacturer