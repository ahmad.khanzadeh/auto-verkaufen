// const axios = require('axios');

// const options = {
//   method: 'GET',
//   url: 'https://cars-by-api-ninjas.p.rapidapi.com/v1/cars',
//   params: {model: 'corolla'},
//   headers: {
//     'X-RapidAPI-Key': '7ff9bfd15dmshb8eaa47dfdb7382p19ee77jsnd1b46c15f4fa',
//     'X-RapidAPI-Host': 'cars-by-api-ninjas.p.rapidapi.com'
//   }
// };

// try {
// 	const response = await axios.request(options);
// 	console.log(response.data);
// } catch (error) {
// 	console.error(error);
// }

export async function fetchCars(){
    // attention: for next time do not write down headers in the header section again
    const headers={
            'X-RapidAPI-Key': '7ff9bfd15dmshb8eaa47dfdb7382p19ee77jsnd1b46c15f4fa',
            'X-RapidAPI-Host': 'cars-by-api-ninjas.p.rapidapi.com'
    }
    const response= await fetch('https://cars-by-api-ninjas.p.rapidapi.com/v1/cars?model=corolla', {
        headers: headers,
    });

    const result =await response.json();

    return result;
}